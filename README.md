# Padrões do Projeto #

Esse documento possui o básico da pradronização dos projetos da equipe

### Indice ###

* Downloads
* Estrutura do Projeto
* Nomes de Funções e Classes
* Nomes dos ID's
* Nomes das Exceptions
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Downloads ###

* http://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/keplerr (ECLIPSE)
* https://git-scm.com/downloads (GIT)
* https://ucbdevelop.hipchat.com

### Estrutura do Projeto ###

![Workspace.PNG](https://bitbucket.org/repo/BrRkgA/images/2134858634-Workspace.PNG)

* Src :  Pasta com arquivos fonte
* Controller : Pacote de Regras de negócio    
               Ex.: Adicionar, Excluir...
* Model :      Pacote de  Modelos   
               Ex.: Proprietario, Usuario...
* View :       Pacote de Views   
               Funções que controlam as “Telas”
* Tests :      Pasta com os arquivos de testes


### Nomes de Funções e Classes ###

* A definir

### Nomes dos ID's ###

* A definir

### Nomes das Exceptions ###

* A definir